import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilesEComponent } from './utiles-e.component';

describe('UtilesEComponent', () => {
  let component: UtilesEComponent;
  let fixture: ComponentFixture<UtilesEComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UtilesEComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilesEComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
